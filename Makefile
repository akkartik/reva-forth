REVA=bin/reva
CORE=bin/core
BUILD=bin/build
GCC=gcc

.PHONY: all test bench

all: $(CORE) $(REVA)

$(CORE): src/core.asm src/brieflz.asm  src/common.asm src/linux.asm
	nasm -Ox -o core.o -DOS=1 -felf src/core.asm
	$(GCC) -s -nostartfiles -o $(CORE) core.o -ldl -m32

$(REVA): src/reva.f $(CORE)
	$(CORE) $<

test: $(REVA)
	$(REVA) tests/test.f
	$(REVA) -t -n math/floats
	$(REVA) -t -n date/parse

bench: $(REVA)
	(cd bench; ../$(REVA) bench.f)

clean:
	-rm $(CORE) $(REVA) *.o
